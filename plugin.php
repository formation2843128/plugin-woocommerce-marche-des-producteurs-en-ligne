<?php
/*
Plugin Name: Marché des producteurs en ligne
Description: Plugin WooCommerce pour créer un marché des producteurs en ligne.
Version: 1.0
Author: Votre Nom
*/

// Enregistrement du post type Producteurs
function marche_producteurs_post_type() {
    $labels = array(
        'name'               => 'Producteurs',
        'singular_name'      => 'Producteur',
        'menu_name'          => 'Producteurs',
        'name_admin_bar'     => 'Producteur',
        'add_new'            => 'Ajouter Nouveau',
        'add_new_item'       => 'Ajouter Nouveau Producteur',
        'new_item'           => 'Nouveau Producteur',
        'edit_item'          => 'Éditer Producteur',
        'view_item'          => 'Voir Producteur',
        'all_items'          => 'Tous les Producteurs',
        'search_items'       => 'Rechercher des Producteurs',
        'parent_item_colon'  => 'Parent Producteurs:',
        'not_found'          => 'Aucun producteur trouvé.',
        'not_found_in_trash' => 'Aucun producteur trouvé dans la corbeille.'
    );

    $args = array(
        'labels'             => $labels,
        'description'        => 'Producteurs pour le marché en ligne.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'producteurs' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'         => array( 'category', 'post_tag' ),
    );

    register_post_type( 'producteurs', $args );
}
add_action( 'init', 'marche_producteurs_post_type' );

// Ajouter des champs personnalisés pour les producteurs et les produits avec ACF
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6235e0c6522b0',
        'title' => 'Informations Producteur',
        'fields' => array(
            array(
                'key' => 'field_6235e0d73e303',
                'label' => 'Photo du Producteur',
                'name' => 'photo_producteur',
                'type' => 'image',
                'instructions' => 'Ajouter la photo du producteur.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
            array(
                'key' => 'field_6235e14d3e304',
                'label' => 'Biographie du Producteur',
                'name' => 'biographie_producteur',
                'type' => 'textarea',
                'instructions' => 'Ajouter la biographie du producteur.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
            array(
                'key' => 'field_6235e1a93e305',
                'label' => 'Localisation du Producteur',
                'name' => 'localisation_producteur',
                'type' => 'text',
                'instructions' => 'Ajouter la localisation du producteur.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'producteurs',
                ),
            ),
        ),
    ));

    acf_add_local_field_group(array(
        'key' => 'group_6235e30077c5f',
        'title' => 'Informations Produit',
        'fields' => array(
            array(
                'key' => 'field_6235e314b0e11',
                'label' => 'Disponibilité Saisonnière',
                'name' => 'disponibilite_saisonniere',
                'type' => 'text',
                'instructions' => 'Ajouter la disponibilité saisonnière du produit.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
            array(
                'key' => 'field_6235e33ab0e12',
                'label' => 'Variétés',
                'name' => 'varietes',
                'type' => 'text',
                'instructions' => 'Ajouter les variétés du produit.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
            array(
                'key' => 'field_6235e353b0e13',
                'label' => 'Certifications Bio',
                'name' => 'certifications_bio',
                'type' => 'text',
                'instructions' => 'Ajouter les certifications bio du produit.',
                'required' => 1,
                'wrapper' => array(
                    'width' => '50',
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'product',
                ),
            ),
        ),
    ));

endif;

// Afficher le profil du producteur sur les pages de produits
function afficher_profil_producteur() {
    global $product;
    if ( is_product() ) {
        $product_id = $product->get_id();
        $producteur_id = get_post_meta( $product_id, 'producteur_associé', true );
        if ( $producteur_id ) {
            $producteur_photo = get_field( 'photo_producteur', $producteur_id );
            $producteur_biographie = get_field( 'biographie_producteur', $producteur_id );
            $producteur_localisation = get_field( 'localisation_producteur', $producteur_id );

            $profil_html = '<div class="profil-producteur">';
            $profil_html .= '<h3>Producteur:</h3>';
            if ( $producteur_photo ) {
                $profil_html .= '<img src="' . esc_url( $producteur_photo['url'] ) . '" alt="' . esc_attr( $producteur_photo['alt'] ) . '" />';
            }
            if ( $producteur_biographie ) {
                $profil_html .= '<p><strong>Biographie:</strong> ' . esc_html( $producteur_biographie ) . '</p>';
            }
            if ( $producteur_localisation ) {
                $profil_html .= '<p><strong>Localisation:</strong> ' . esc_html( $producteur_localisation ) . '</p>';
            }
            $profil_html .= '</div>';
            echo $profil_html;
        }
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_profil_producteur', 5 );

// Afficher les informations supplémentaires d'un produit sur sa page
function afficher_infos_supplementaires_produit() {
    global $product;
    if ( is_product() ) {
        $disponibilite_saisonniere = get_field( 'disponibilite_saisonniere', $product->get_id() );
        $varietes = get_field( 'varietes', $product->get_id() );
        $certifications_bio = get_field( 'certifications_bio', $product->get_id() );

        $infos_html = '<div class="infos-supplementaires-produit">';
        if ( $disponibilite_saisonniere ) {
            $infos_html .= '<p><strong>Disponibilité saisonnière:</strong> ' . esc_html( $disponibilite_saisonniere ) . '</p>';
        }
        if ( $varietes ) {
            $infos_html .= '<p><strong>Variétés:</strong> ' . esc_html( $varietes ) . '</p>';
        }
        if ( $certifications_bio ) {
            $infos_html .= '<p><strong>Certifications Bio:</strong> ' . esc_html( $certifications_bio ) . '</p>';
        }
        $infos_html .= '</div>';
        echo $infos_html;
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_infos_supplementaires_produit', 10 );

// Afficher une recette sur la page produit seulement si elle a été achetée
function afficher_recette_si_achete() {
    global $product;
    if ( is_product() ) {
        if ( $product->is_purchasable() && $product->is_in_stock() ) {
            echo '<div class="recette">Recette</div>';
        } else {
            echo '<p>Vous devez acheter ce produit pour voir la recette.</p>';
        }
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_recette_si_achete', 15 );

?>
