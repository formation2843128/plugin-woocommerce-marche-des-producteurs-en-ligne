<?php

function afficher_profil_producteur() {
    global $product;
    if ( is_product() ) {
        $product_id = $product->get_id();
        $producteur_id = get_post_meta( $product_id, 'producteur_associé', true );
        if ( $producteur_id ) {
            $producteur_photo = get_field( 'photo_producteur', $producteur_id );
            $producteur_biographie = get_field( 'biographie_producteur', $producteur_id );
            $producteur_localisation = get_field( 'localisation_producteur', $producteur_id );

            $profil_html = '<div class="profil-producteur">';
            $profil_html .= '<h3>Producteur:</h3>';
            if ( $producteur_photo ) {
                $profil_html .= '<img src="' . esc_url( $producteur_photo['url'] ) . '" alt="' . esc_attr( $producteur_photo['alt'] ) . '" />';
            }
            if ( $producteur_biographie ) {
                $profil_html .= '<p><strong>Biographie:</strong> ' . esc_html( $producteur_biographie ) . '</p>';
            }
            if ( $producteur_localisation ) {
                $profil_html .= '<p><strong>Localisation:</strong> ' . esc_html( $producteur_localisation ) . '</p>';
            }
            $profil_html .= '</div>';
            echo $profil_html;
        }
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_profil_producteur', 5 );

function afficher_infos_supplementaires_produit() {
    global $product;
    if ( is_product() ) {
        $disponibilite_saisonniere = get_field( 'disponibilite_saisonniere', $product->get_id() );
        $varietes = get_field( 'varietes', $product->get_id() );
        $certifications_bio = get_field( 'certifications_bio', $product->get_id() );

        $infos_html = '<div class="infos-supplementaires-produit">';
        if ( $disponibilite_saisonniere ) {
            $infos_html .= '<p><strong>Disponibilité saisonnière:</strong> ' . esc_html( $disponibilite_saisonniere ) . '</p>';
        }
        if ( $varietes ) {
            $infos_html .= '<p><strong>Variétés:</strong> ' . esc_html( $varietes ) . '</p>';
        }
        if ( $certifications_bio ) {
            $infos_html .= '<p><strong>Certifications Bio:</strong> ' . esc_html( $certifications_bio ) . '</p>';
        }
        $infos_html .= '</div>';
        echo $infos_html;
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_infos_supplementaires_produit', 10 );

function afficher_recette_si_achete() {
    global $product;
    if ( is_product() ) {
        if ( $product->is_purchasable() && $product->is_in_stock() ) {
            // Afficher la recette ici
            echo '<div class="recette">Recette</div>';
        } else {
            echo '<p>Vous devez acheter ce produit pour voir la recette.</p>';
        }
    }
}
add_action( 'woocommerce_before_single_product_summary', 'afficher_recette_si_achete', 15 );
