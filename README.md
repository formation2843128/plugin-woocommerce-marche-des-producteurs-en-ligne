# Marché des producteurs en ligne WooCommerce

## Étape 1 : Configuration des producteurs et des produits

1. Créez des producteurs en naviguant dans la section "Producteurs" dans le menu de gauche de votre tableau de bord WordPress.
2. Pour chaque producteur, remplissez les champs personnalisés tels que la photo, la biographie et la localisation.
3. Ensuite, créez ou éditez des produits dans WooCommerce.
4. Remplissez les champs personnalisés pour chaque produit, tels que la disponibilité saisonnière, les variétés et les certifications bio.

## Étape 2 : Utilisation des shortcodes

- Sur les pages où vous souhaitez afficher le profil du producteur, utilisez le shortcode `[profil_producteur]`.
- Sur les pages où vous souhaitez afficher les informations supplémentaires du produit, utilisez le shortcode `[infos_supplementaires_produit]`.
- La recette sera automatiquement affichée sur la page du produit si elle est achetée.
